import { combineReducers } from 'redux';
import tables from './tableReducer';

const rootReducer = combineReducers({
  tables
});

export default rootReducer;
