import { assign } from 'lodash';
import * as types from '../constants/actionTypes';
import initialState from '../constants/initialState';
export default function tableReducers(state = initialState, action) {
  switch (action.type) {
    case types.CREATE_TABLE_SUCCESS:
      return assign({}, state, {
        tables:[...state.tables,{table_name: action.table_name}],
        table: action.table
      });
    case types.LOAD_TABLE_SUCCESS:
      return assign({}, state, {
        table: action.table,
        tableObj: action.tableObj
      })
    case types.LOAD_TABLES_SUCCESS:
      return assign({}, state, {
        tables: action.tables
      })
    case types.UPDATE_FIELD:
      return assign ({}, state , { [action.name]:action.value });
    default:
      return state;
  }
}
