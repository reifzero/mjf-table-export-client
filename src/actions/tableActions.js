import * as types from '../constants/actionTypes';
import TableApi from '../api/tableApi';

export function createTableSuccess (table, table_name) {
  return { type: types.CREATE_TABLE_SUCCESS, table, table_name };
}

export function loadTableSuccess (table ,tableObj) {
  return { type: types.LOAD_TABLE_SUCCESS, table, tableObj };
}

export function loadTablesSuccess (tables) {
  return { type: types.LOAD_TABLES_SUCCESS, tables };
}

export function loadCsvSuccess (csv) {
  return { type:  types.LOAD_CSV_SUCCESS, csv };
}

export function updateField (name, value) {
  return { type: types.UPDATE_FIELD, name, value}
}

export function loadTables() {
  return function (dispatch) {
    return TableApi.getAllTables().then((tables) => {
      dispatch(loadTablesSuccess(tables));
    }).catch((error) => {
      throw(error);
    });
  };
}
export function loadTable(tableName) {
  return function (dispatch) {
    return TableApi.getTable(tableName).then(tableObj => {
      TableApi.convertToCsv(tableObj).then(csv => {
        dispatch(loadTableSuccess(csv, tableObj));
      })
    }).catch(error => {throw(error)});
  }
}

export function processFile(file) {
  return function (dispatch) {

    return TableApi.convertFileToJson(file).then(tableObj => {
      TableApi.convertToCsv(tableObj).then(csv => {
        dispatch(loadTableSuccess(csv, tableObj));
      }).catch(error => { throw(error) });
    });
  }
}

export function saveTable(table, table_name) {
  return function (dispatch) {
    return TableApi.convertToJson(table).then(tableObj => {
      TableApi.saveTable(tableObj, table_name).then(newTable => {
        dispatch(createTableSuccess(table, table_name));
      }).catch(error => { throw(error) });
    })
  };
}

export function convertToCsv(json){
  return function (dispatch) {
    return TableApi.convertToCsv(json).then(csv => {
      dispatch(loadCsvSuccess(csv));
    }).catch(error => { throw(error) });
  }
}

export function convertToJson(csv) {
  return function (dispatch) {
    return TableApi.convertToJson(csv).then(table => {
      dispatch(loadTableSuccess(table));
    }).catch(error => { throw(error) });
  }
}
