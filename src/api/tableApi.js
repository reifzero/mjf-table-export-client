import axios from 'axios';
import json2csv from 'json2csv';

class TableApi {
  static getAllTables() {
    return new Promise((resolve, reject) => {
      axios.get('/api/tables')
      .then(response => {
        resolve(response.data);
      }).catch(error => {
        reject(error);
      });
    });
  }

  static getTable(name) {
    return new Promise((resolve, reject) => {
      axios.get(`/api/tables/${name}`)
      .then(response => {
        resolve(response.data);
      }).catch(error => {
        reject(error);
      });
    });
  }

  static saveTable(data,table_name) {
    return new Promise((resolve, reject) => {
        axios.post('/api/tables',{data, table_name})
        .then(response => {
          resolve(response.data);
        }).catch(error => {
          reject(error);
        });
    });
  }

  static convertToJson(data) {
    return new Promise((resolve, reject) => {
      axios.post('/api/csv',{ csv:data })
      .then(response => {
        resolve(response.data);
      }).catch(error => {
        reject(error);
      });
    });
  }

  static convertFileToJson(data) {
    return new Promise((resolve, reject) => {
      axios.post('/api/csv',data)
      .then(response => {
        resolve(response.data);
      }).catch(error => {
        reject(error);
      });
    });
  }

  static convertToCsv(table) {
    return new Promise((resolve, reject) => {
      json2csv({data: table},(error,result) =>{
        if(error){
          reject(error);
        }else{
          resolve(result);
        }
      })
    });
  }

}

export default TableApi;
