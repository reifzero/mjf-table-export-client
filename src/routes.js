import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/App';
import HomePage from './components/home/HomePage';
import ImportTablePage from './components/ImportTablePage';
import ExportTablePage from './components/ExportTablePage';

export default (
  <Route path='/' component = { App }>
    <IndexRoute component = {HomePage} />
    <Route path = 'import' component = { ImportTablePage } />
    <Route path = 'export' component = { ExportTablePage } />
  </Route>
);
