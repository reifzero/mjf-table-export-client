import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';
import { Navbar, NavItem, Nav } from 'react-bootstrap';
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap'
const Header = () => {
  return (
    <Navbar>
      <Nav bsStyle='pills'>
        <IndexLinkContainer to = '/' activeClassName='active'>
          <NavItem>Home</NavItem>
        </IndexLinkContainer>
        <LinkContainer to = '/export' activeClassName='active'>
          <NavItem>Export Tables</NavItem>
        </LinkContainer>
        <LinkContainer to = '/import' activeClassName='active'>
          <NavItem>Import Tables</NavItem>
        </LinkContainer>
      </Nav>
    </Navbar>
  )
}
export default Header;
