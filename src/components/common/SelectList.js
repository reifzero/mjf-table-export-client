import React, { PropTypes } from 'react';
import { FormControl, FormGroup, ControlLabel } from 'react-bootstrap';

const SelectList = ({ name, label, defaultOption, onChange, value, error, options }) => {
  return (
  <FormGroup>
    <ControlLabel>{ label }</ControlLabel>
    <FormControl
      name = { name }
      onChange = { onChange }
      value = { value }
      componentClass = 'select'
      placeholder = { defaultOption }>
      { options.map( (option) => {
        return (
          <option key = {option.value} value = {option.value}>
            { option.text }
          </option>
        );
      }) }
    </FormControl>
  </FormGroup>);
};

SelectList.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  options: PropTypes.array.isRequired,
  defaultOption: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string
}

export default SelectList;
