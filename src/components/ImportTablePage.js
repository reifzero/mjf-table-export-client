import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Panel, FormGroup, FormControl, ControlLabel , Button } from 'react-bootstrap';
import * as tableActions from '../actions/tableActions';
import TableList from './TableList'
import DropZone from 'react-dropzone';

class ImportTablePage extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      tables:{
        table: this.props.table || '',
        tableObj: this.props.tableObj || [],
        table_name: ''
      }
    }
    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
    this.onDrop = this.onDrop.bind(this);
  }
  onDrop (files) {
    this.props.actions.processFile(files[0]);
  }
  onChange (event) {
    const { name, value, type } = event.target;
    this.props.actions.updateField(name, value);

  }

  onClick (event) {
    const { table, table_name } = this.props;
    this.props.actions.saveTable(table, table_name);
  }

  render () {
    const {  table, tableObj, table_name  } = this.props;
    return (
    <Panel header = "Import Tables">
      <form >
        <FormGroup controlId="table_name">
          <ControlLabel>Table Name</ControlLabel>
          <FormControl
            name="table_name"
            type="text"
            placeholder="Enter Table Name"
            value={ table_name }
            onChange={ this.onChange } />
        </FormGroup>
        <DropZone
          style = { {
             width: 'inherit',
             height:'inherit'
             } }
          ref="dropzone"
          onDrop = { this.onDrop }
          disableClick = { true }
          multiple = { false }>
        <TableList
          table= { table }
          tableObj = {tableObj}
          onChange = {this.onChange}
          placeholder = 'Enter Text or Drag a file here...' />
        </DropZone>
        <Button bsStyle ="primary" onClick = { this.onClick }>Create Table</Button>
      </form>
    </Panel>);
  }
}

ImportTablePage.propTypes = {
  table: PropTypes.string.isRequired
}

function mapStateToProps(state, ownProps){
  return {
     table_name: state.tables.table_name,
     table: state.tables.table,
     tableObj: state.tables.tableObj
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(tableActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (ImportTablePage);
