import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Panel } from 'react-bootstrap';
import * as tableActions from '../actions/tableActions';
import SelectList from './common/SelectList';
import TableList from './TableList';
import { assign, isEmpty } from 'lodash';

class ExportTablePage extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.onSave = this.onSave.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange (event) {
    const { name, value, type } = event.target;
    if(name === 'tables'){
      this.props.actions.loadTable(value);
    }
  }

  onSave () {
    this.props.actions.createTable(this.state.table);
    const table = {};
    this.setState({ table });
  }

  render () {
    const { tables, table, tableObj, table_name } = this.props;
    return (
      <Panel header="Export Tables">
        <SelectList
          name = 'tables'
          label = 'Tables'
          defaultOption = { table_name }
          onChange = { this.onChange }
          options = { tables }
          />
        { isEmpty(table) ? '' : <hr /> }
        { isEmpty(table) ? '' : <TableList
          table= { table }
          tableObj = {tableObj}
          onChange = {this.onChange} /> }

      </Panel>
    ) ;
  }
}

ExportTablePage.propTypes = {
  table: PropTypes.string.isRequired,
  tables: PropTypes.array.isRequired
};

function mapStateToProps(state, ownProps){
  const tablesFormattedForDropdown = state.tables.tables.map(table => {
    return {
      value: table.table_name,
      text: table.table_name
    };
  });
  return {
    tables: tablesFormattedForDropdown,
    table: state.tables.table,
    table_name: state.tables.table_name,
    tableObj:  state.tables.tableObj
  }
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(tableActions, dispatch)
  }
};

export default connect(mapStateToProps, mapDispatchToProps) (ExportTablePage);
