import React from 'react';
import { Link } from 'react-router';
import { Well } from 'react-bootstrap';
class HomePage extends React.Component {
  render () {
    return (
      <Well>
        <h2>Table Exporter</h2>
        <hr/>
        <dl>
          <dt>Author</dt>
          <dd>Robin Leenders</dd>
          <dt>Architecture</dt>
          <dd>React/Redux Frontend, Lumen webservice API, MariaDB database</dd>
          <dt>Details</dt>
          <dd>This App queries the database to retrieve all the tables in a particular
          Schema, and will then retrieve a csv style representation of the table and post it to the client
          from there, the user can choose to either re-import the table, or import an entirely new table in the import tab.
          </dd>
          <dt>Methods of Importing tables</dt>
          <dd>you can populate the table field by either exporting an existing table (in the export tab),
            by copying and pasing CSV into the import text area, or by simply draggin a csv into the text area
          </dd>
          <dt>Potential Improvements</dt>
          <dd>
            <ul>
              <li>Add csv validation</li>
              <li>Add server side request validation, and enhanced error handling</li>
              <li>Add A download button to the export page</li>
              <li>Provide a placeholder value to the tables dropdown </li>
              <li>Select the imported table after navigating to the export page</li>
              <li>Allow the user to see the export as JSON</li>
              <li>Implement Success/Fail notifications</li>
            </ul>
          </dd>
        </dl>
      </Well>
    );
  }
}

export default HomePage;
