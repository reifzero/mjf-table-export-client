import React, { PropTypes } from 'react';
import Header from './common/Header';
import { Grid } from 'react-bootstrap';

class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Grid>
          { this.props.children }
        </Grid>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired
};

export default App;
