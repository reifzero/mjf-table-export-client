import React, { PropTypes } from 'react';
import { FormGroup, FormControl, ControlLabel } from 'react-bootstrap';

const TableList = ({ table, tableObj, onChange, placeholder }) => {
    return (
      <FormGroup controlId="table">
        <ControlLabel>Table Contents</ControlLabel>
        <FormControl
          name="table"
           componentClass="textarea"
            placeholder= { placeholder }
            value={ table }
            rows = { tableObj? tableObj.length + 5 : 5 }
            onChange = {onChange} />
      </FormGroup>
    );
}

TableList.propTypes = {
  table: PropTypes.string.isRequired,
  tableObj: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
}

export default TableList;
